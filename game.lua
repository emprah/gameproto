require "math"
require "string"
require "os"

local Entity = {name='', hp=30, items=nil, weapon=nil, armor=nil }
function Entity:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function Entity:isDead ()
   return self.hp <=0 and true or false
end

function Entity:useItem (id, entity)
   if self.items[id] then
      return self.items[id]:use(entity)
   end
end

function Entity:attack (entity)
   print(" Weapon dmg ----")
   local dmgs = self.weapon:use()
   for k, v in pairs(dmgs) do print('  DMG', k, v) end

   print(" Resists ----")
   dmgs = entity:resist(dmgs)
   
   print(' Damage caused ----')
   for k,v in pairs(dmgs) do print('\t',k, v) end
   entity:sufferDmg(dmgs)
end

function Entity:sufferDmg (dmgs)
   local acc = 0
   for _, dmg in pairs(dmgs) do
      acc = acc + dmg
   end
   self.hp = self.hp - acc
   if self.hp < 0 then self.hp = 0 end
end

function Entity:resist (dmgs)
   for element, dmg in pairs(dmgs) do
      for _, armorelm in ipairs(self.armor) do
	 if armorelm.damage[element] then
	    for _, roll in pairs(armorelm.damage[element]) do
	       dmgs[element] = dmg - roll:rollDmg()
	    end
	 end
      end
      if dmgs[element] < 0 then dmgs[element] = 0 end
   end
   return dmgs
end


local BasicDice = {name='generic dice',sides=1}
function BasicDice:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function BasicDice:roll (entity)
  -- math.randomseed(os.time())
   return math.random(self.sides)
end

local D6 = BasicDice:new{name='d6', sides=6}
local D12 = BasicDice:new{name='d12', sides=12}
local D20 = BasicDice:new{name='d20', sides=20}

local Damage = {dice=nil, mod=0 }
function Damage:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function Damage:rollDmg ()
   local dmg = 0
   if self.dice then dmg = self.dice:roll() end
   dmg = dmg + self.mod
   if dmg < 1 then dmg = 1 end
   print('  dice roll:', dmg - self.mod, self.mod)
   return dmg
end

local Weapon = {name='', damage=nil}
function Weapon:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function Weapon:use ()
   local dmgpertype = {}
   for element, dmgroll in pairs(self.damage) do
      if dmgpertype[element] == nil then dmgpertype[element] = 0 end
      for _, roll in pairs(dmgroll) do
	 dmgpertype[element] = dmgpertype[element] + roll:rollDmg()
      end
   end
   --      print(element, dmgs[element])
   return dmgpertype
end   

local BroadSword = Weapon:new{name='Broad Sword', damage={['neutral']={Damage:new{dice=D12, mod=2}}}}
local FireBall = Weapon:new{name='Fire Ball', damage={['fire']={Damage:new{dice=D6, mod=5}}}}


local Armor = {damage=nil}
function Armor:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

local LeatherArmor = Armor:new{name='Leather Armor', damage={['neutral']={Damage:new{dice=nil, mod=5}},
							     ['fire']={Damage:new{dice=D6, mod=-2}}}}
local HelmOfFire = Armor:new{name='Helm Of Fire', damage={['fire']={Damage:new{dice=nil, mod=3}}}}

local BasicItem = {name='generic item', amount=1}
function BasicItem:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

HealthPotion10 = BasicItem:new{name='Health Potion +10'}
function HealthPotion10:use (entity)
   if self.amount < 1 then return end
   entity.hp = entity.hp + 10
   self.amount = self.amount - 1
end


local GameState = { turn=0, quit=false }
function GameState:new (o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function GameState:incrementTurn ()
   self.turn = self.turn + 1
end

function GameState:getCmd ()
   return string.gsub(io.read(), '\n', '')
end

function GameState:start ()
   print([[Welcome to GameProto\n A Goblin attacks you!]])
   while true do
      print('###Turn:', self.turn, '###')
      self.cmd = self:getCmd()
      self[self.cmd](self)
      if self.cmd == 'quit' then break end
      self:mobturn()
      self:incrementTurn()
   end
end

function GameState:quit ()
   self.quit = true
   print('Goodbye')
end

function GameState:attack ()
   print('-hero attacks-')
   self.heros[1]:attack(self.mobs[1])
   print('goblin health:', self.mobs[1].hp)
   if self.mobs[1]:isDead() then print('u win'); os.exit() end
   return
end

function GameState:heal()
   print('-hero uses health potion-')
   self.heros[1].items['HealthPotion+10']:use(self.heros[1])
   print('hero health:', self.heros[1].hp)
   return
end

function GameState:mobturn ()
   print('-mob attacks-')
   self.mobs[1]:attack(self.heros[1])
   print('hero health:', self.heros[1].hp)
   if self.heros[1]:isDead() then print('u dead'); os.exit() end
   return
end

math.randomseed(os.time())
local hero1 = Entity:new{weapon=FireBall, armor={LeatherArmor}, items={['HealthPotion+10']=HealthPotion10:new()}}
local goblin1 = Entity:new{weapon=BroadSword, armor={LeatherArmor, HelmOfFire}}
local g = GameState:new{heros={hero1}, mobs={goblin1}}
g:start()
